# Secret Hitler print n play

## Automatic built pdf's

[📥 secret_hitler.zip](../-/jobs/artifacts/master/raw/secret_hitler.zip?job=build)
[📥 trump_pack.pdf](../-/jobs/artifacts/master/raw/trump_pack.pdf?job=build)

## Build locally

### Dependencies
* [GNU make](https://www.gnu.org/software/make/)
* [inkscape](https://inkscape.org/) >= 1.0.0 🥰
* [ghostscript](https://www.ghostscript.com/)
If you use the environment mentioned below, you also need
* [sed](https://www.gnu.org/software/sed/)

To install the dependencies on a debian based OS:

```bash
sudo apt-get install inkscape make ghostscript
```

### Generate pdfs

```bash
make
```

Chose retro style instead of original style role backcovers:

```bash
ROLES_BACK=retro make
```

Chose original colorization of the role cards (also
`ROLES_COLORIZATION=grey` is possible):

```bash
ROLES_COLORIZATION=original make
```

The above environment variables can be combined.

## Modify

You will need the following fonts

* AHAMONO Monospaced
* Eskapade Fraktur W04 Black
* Germania
* Futura Hv BT
* Futura Bold

AHAMONO Monospaced is embedded in the official rules pdf.
You can extract it e.g. using [fontforge](https://fontforge.org).
