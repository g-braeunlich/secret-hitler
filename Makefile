all: pdf pdf/secret_hitler.pdf pdf/board-a4.pdf pdf/board-a3.pdf pdf/envelopes.pdf

ifeq ($(ROLES_BACK), retro)
roles_back_suffix = -retro
endif

ifdef ROLES_COLORIZATION
roles_color_suffix = -$(ROLES_COLORIZATION)
endif

pages = roles-fascists$(roles_color_suffix) \
        roles-back-fascists$(roles_back_suffix) \
        roles-liberals$(roles_color_suffix) \
        roles-back$(roles_back_suffix) \
	party_membership \
	party_membership-back \
	articles \
	articles-back \
	ballot-ja \
	ballot-back \
	ballot-nein \
	ballot-back \
	placards

pdf/secret_hitler.pdf: $(addprefix .build/,$(addsuffix .pdf,$(pages)))
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -dAutoRotatePages=/None -sOutputFile="$@" $+

pdf/envelopes.pdf: .build/envelope.pdf
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -dAutoRotatePages=/None -sOutputFile="$@" $< $< $< $<

pdf/board-a4.pdf: .build/board-a4-l.pdf .build/board-a4-r.pdf
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -dAutoRotatePages=/None -sOutputFile="$@" $^

pdf/board-a3.pdf: .build/board.pdf
	cp "$<" "$@"

.build/board-a4-l.pdf: .build/board.pdf
	gs -o "$@" -sDEVICE=pdfwrite -c "[/CropBox [60 0 655 842]" -c " /PAGES pdfmark" -f "$<"
.build/board-a4-r.pdf: .build/board.pdf
	gs -o "$@" -sDEVICE=pdfwrite -c "[/CropBox [535 0 1130 842]" -c " /PAGES pdfmark" -f "$<"

.build/roles-fascists-original.svg: roles-fascists.svg
	sed -e '/\.role/s/mix-blend-mode:hard-light/mix-blend-mode:multiply/g' -e '/\.background-role/s/fill:#[a-f0-9]\{6\}/fill:#f7e1c1/g' $< > $@
.build/roles-liberals-original.svg: roles-liberals.svg
	sed -e '/\.role/s/mix-blend-mode:hard-light/mix-blend-mode:multiply/g' -e '/\.background-role/s/fill:#[a-f0-9]\{6\}/fill:#f7e1c1/g' $< > $@
.build/roles-fascists-grey.svg: roles-fascists.svg
	sed -e '/\.role/s/mix-blend-mode:hard-light/mix-blend-mode:normal/g' -e '/\.background-role/s/fill:#[a-f0-9]\{6\}/fill:#ffffff/g' $< > $@
.build/roles-liberals-grey.svg: roles-liberals.svg
	sed -e '/\.role/s/mix-blend-mode:hard-light/mix-blend-mode:normal/g' -e '/\.background-role/s/fill:#[a-f0-9]\{6\}/fill:#ffffff/g' $< > $@


.build/%.pdf: %.svg
	mkdir -p .build
	inkscape $(INKSCAPE_ARGS) --export-ignore-filters --export-filename="$@" "$<"
.build/%.pdf: .build/%.svg
	mkdir -p .build
	inkscape $(INKSCAPE_ARGS) --export-ignore-filters --export-filename="$@" "$<"

pdf:
	mkdir -p pdf
